import java.util.*;
import java.io.*;

public class Deterministic extends FiniteAutomata {


    public Deterministic(){
        super();
    }

    public Deterministic(NonDeterministic ndfa){
        super();
        buildFrom(ndfa);
    }

    private void buildFrom(NonDeterministic ndfa){
        alphabet = ndfa.alphabet;
        alphabet.remove("lmd");

        // Create state
        State initial = new State("Q0");

        // Populate state with NDFA initial state
        initial.addState(ndfa.get(ndfa.initialState));

        // Add state to DFA states
        states.add(initial);

        // Set it as initial
        setInitial(initial);

        // Lambda closure
        getLambdaClosure(ndfa);

        // NDFA-DFA state transitions
        getStates(ndfa);

        // Get final states
        getFinalStates(ndfa);
    }

    private void getLambdaClosure(NonDeterministic ndfa){
        // Get reference list of NDFA states
        ArrayList<State> ndfa_states = ndfa.states;

        // Set default lambda transition to every state in reference list
        for(State state : ndfa_states){
            state.addTransition("lmd", state.id);
        }
    }

    private void getStates(NonDeterministic ndfa){
        // Iterate through every DFA state
        int count = 0;
        while(count < states.size()){


            State current_dfa_state = states.get(count);

            // For every letter in the alphabet get current_dfa_state
            // transitions
            for(String current_letter : this.alphabet){


                // Reference list of all destinations to current_letter
                ArrayList<State> l_destinations = new ArrayList<State>();

                // Decompose DFA state
                for(State state : current_dfa_state.states){

                    // Get all state's destinations from current_letter
                    ArrayList<String> s_destinations = state.getAllDestinationsFrom(current_letter);

                    // Loop through every destination
                    for(String t_state : s_destinations){

                        // If destination is not already on l_destinations
                        // add it, else continue
                        if(!l_destinations.contains(ndfa.get(t_state))){
                            l_destinations.add(ndfa.get(t_state));
                        }

                        // Get destination's lambda destinations
                        ArrayList<String> lmd_destination = ndfa.get(t_state).getAllDestinationsFrom("lmd");


                        for(String l_state : lmd_destination){

                            // If destination is not already on l_destinations
                            // add it, else continue
                            if(!l_destinations.contains(ndfa.get(l_state))){
                                l_destinations.add(ndfa.get(l_state));
                            }

                        }
                    }
                }

                // Create temporal dfa state for reference
                State temp_dfa = new State("Q" + (this.states.size()));


                // Populate DFA state with every destination of current_letter
                // if there are no destinations populate with err (empty)
                if(l_destinations.size() != 0){
                    for(State n_state : l_destinations){
                        temp_dfa.addState(n_state);
                    }
                }else{
                    temp_dfa.addState(new State("err"));
                }

                // Check if temp_state already exists
                boolean exists = false;
                int existing_position = -1;
                for(State e_state : this.states){
                    existing_position++;

                    if(e_state.matches((State)temp_dfa) == true){

                        exists = true;
                        break;
                    }
                }

                // If temp_state does not exist yet, add it and then add
                // transition from current_dfa_state to temp_state
                // else, get existing state, add transition from
                // current_dfa_state
                if(!exists){

                    // Add new dfa state
                    this.states.add(temp_dfa);


                    // Assign transition to current_dfa_state to temp_state
                    current_dfa_state.addTransition(current_letter, temp_dfa.id);


                }else{

                    // Get dfa state
                    State existing_state = (State) this.states.get(existing_position);

                    // Assign transition to current dfa to existing one
                    current_dfa_state.addTransition(current_letter, existing_state.id);

                    // Reasign modified dfa state to states
                    this.states.set(existing_position, existing_state);
                }
            }
            count++;
        }
    }

    private void getFinalStates(NonDeterministic ndfa){
        ArrayList<String> ndfa_finals = ndfa.finalStates;

        for(State dfa_state : this.states){
            for(State state : dfa_state.states){


                if(ndfa_finals.contains(state)){
                    this.setFinal(dfa_state);
                }
            }
        }
    }
}
