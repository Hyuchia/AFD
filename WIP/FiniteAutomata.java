import java.util.*;
import java.io.*;

public class FiniteAutomata implements Automata {

    public ArrayList<State> states;
    public ArrayList<String> finalStates;
    public ArrayList<String> alphabet;
    public String initialState;

    public FiniteAutomata(){
        states = new ArrayList<State>();
        alphabet = new ArrayList<String>();
        finalStates = new ArrayList<String>();
    }

    @Override
    public void addSymbol(String symbol){
        alphabet.add(symbol);
    }

    @Override
    public void addState(State state){
        states.add(state);
    }

    @Override
    public void addState(String id){
        states.add(new State(id));
    }

    @Override
    public void setInitial(String id){
        get(id).isInitial = true;
        initialState = id;
    }

    @Override
    public void setInitial(State state){
        get(state).isInitial = true;
        initialState = state.id;
    }

    @Override
    public void setFinal(String id){
        get(id).isFinal = true;
        finalStates.add(id);
    }

    @Override
    public void setFinal(State state){
        get(state).isFinal = true;
        finalStates.add(state.id);
    }

    @Override
    public void addTransition(String origin, String symbol, String destination){
        get(origin).addTransition(symbol, destination);
    }

    @Override
    public boolean hasSymbol(String symbol){
        return alphabet.contains(symbol);
    }

    @Override
    public State get(String id){
        return states.get(states.indexOf(new State(id)));
    }

    @Override
    public State get(State state){
        return states.get(states.indexOf(state));
    }

    public String toString(){
        String ndfa = "===============";
        ndfa += "\n" + this.getClass().getSimpleName() + "\n";
        ndfa += "===============";
        ndfa += "\nSTATES:\n";
        ndfa += states + " ";
        ndfa += "\nALPHABET:\n";
        ndfa += alphabet + " ";
        ndfa += "\nINITIAL STATE:\n";
        ndfa += initialState;
        ndfa += "\nFINAL STATES:\n";
        ndfa += finalStates + " ";
        ndfa += "\nTRANSITIONS:\n";
        for(State state : states){
            for(Transition t : state.transitions){
                ndfa += state.id + ' ' + t.toString();
                ndfa += "\n";
            }
        }
        return ndfa;
    }

}
