import java.util.*;

public class Principal {

    public static void main(String[] args){

        // Create ndfa Object
        NonDeterministic ndfa = new NonDeterministic();

        // Fill AFND with File's Contents

        ArrayList<String> contents = File.getAllLines("data.txt");

        // Read all the available states
        String[] states = contents.get(0).split(",");
        for(int i = 0; i < states.length; i++){
            ndfa.addState(states[i]);
        }

        // Read all the symbols for the alphabet and add them
        String[] symbols = contents.get(1).split(",");
        for(int i = 0; i < symbols.length; i++){
            ndfa.addSymbol(symbols[i]);
        }

        // Set the Initial State
        ndfa.setInitial(contents.get(2));

        // Set the list of Final States
        String[] finalStates = contents.get(3).split(",");
        for(int i = 0; i < finalStates.length; i++){
            ndfa.setFinal(finalStates[i]);
        }

        // Set all the transitions
        for(int i = 4; i < contents.size(); i++){
            String line = contents.get(i);
            if (line.indexOf("->") > -1) {
                String[] parts = line.split("->");
                String[] left = parts[0].split(",");
                String[] right = parts[1].split(",");

                for(int j = 0; j < right.length; j++){
                    if(ndfa.hasSymbol(left[1])){
                        ndfa.addTransition(left[0], left[1], right[j]);
                    }
                }

            }
        }

        System.out.println(ndfa);


        Deterministic dfa = new Deterministic(ndfa);

        System.out.println(dfa);

        String print = "";

        String transitions = "";
        for(State state : dfa.states){
            print += state.toString().replace(" ", "").replace("[", "").replace("]", "") + ",";
            for(Transition transition : state.transitions){
                transitions += state.id + "," + transition.symbol + "->" + transition.destination + "\n";
            }
        }
        print = print.substring(0, print.length() -1);
        print += "\n" + dfa.alphabet.toString().replace(" ", "").replace("[", "").replace("]", "") + "\n";

        print += dfa.initialState + "\n";

        for(String state : dfa.finalStates){
            print += state + ",";
        }
        print = print.substring(0, print.length() -1);

        print += "\n" + transitions;

        File.write(print, "result.txt");



    }

}
